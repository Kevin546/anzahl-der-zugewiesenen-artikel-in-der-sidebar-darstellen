<?php declare(strict_types=1);

namespace SidebarArticlesAmount\Subscriber;

use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Content\Category\Event\NavigationLoadedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Content\Category\Tree\TreeItem;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Aggregation\Metric\CountAggregation;
use Shopware\Core\Framework\DataAbstractionLayer\Search\AggregationResult\Metric\CountResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;

class GetCategoryProductsSubscriber implements EventSubscriberInterface
{   

    public function __construct(EntityRepositoryInterface $productRepository) {
        $this->productRepository = $productRepository;
    }

    public static function getSubscribedEvents(): array 
    {
        return [
            NavigationLoadedEvent::class =>  "onCategoriesLoaded"
        ];
    }

    public function onCategoriesLoaded(NavigationLoadedEvent $event)
    {
        
        /** @var TreeItem $treeItem */
        foreach ($event->getNavigation()->getTree() as $treeItem) {

            $totalSubCategoryProductCount = 0;

            foreach ($treeItem->getChildren() as $subTreeItem) {

                $criteria = new Criteria();
                $criteria->addFilter(new EqualsAnyFilter('product.categoryTree', [$subTreeItem->getCategory()->getId()]));
                $criteria->addFilter(new EqualsFilter('product.parentId', null));
                $criteria->addFilter(new EqualsFilter('product.active', 1));
                $criteria->addAggregation(new CountAggregation('productCount', 'product.id'));
        
                /** @var CountResult $productCountResult */
                $productCountResult = $this->productRepository
                    ->search($criteria, $event->getContext())
                    ->getAggregations()
                    ->get('productCount');
        
                $subTreeItem->addExtension('product_count', $productCountResult);

                $intProductCountResult = $productCountResult->getCount();


                $totalSubCategoryProductCount = $totalSubCategoryProductCount + $intProductCountResult;
                }
             
            $criteria2 = new Criteria();
            $criteria2->addFilter(new EqualsAnyFilter('product.categoryTree', [$treeItem->getCategory()->getId()]));
            $criteria2->addFilter(new EqualsFilter('product.parentId', null));
            $criteria2->addFilter(new EqualsFilter('product.active', 1));
            $criteria2->addAggregation(new CountAggregation('productCount', 'product.id'));
    
            $productCountResult = $this->productRepository
                ->search($criteria2, $event->getContext())
                ->getAggregations()
                ->get('productCount');

            $treeItem->addExtension('product_count', $productCountResult);    
   
        }


       
       
    }
}